<?php

require_once('controller/ViewController.php');
require_once('utilities/DependencyResolver.php');
require_once('factory/CompanyContactFactory.php');
require_once('factory/CompanyFactory.php');
require_once('utilities/Router.php');

include('dependencies.php');

$connection = DependencyResolver::getClass('db-connection');
$companyFactory = DependencyResolver::getClass('company-factory');
$companyContactFactory = DependencyResolver::getClass('company-contact-factory');

$companyRepository = DependencyResolver
    ::getClassWithDependency('company-repository', [$connection, $companyFactory]);

$companyContactRepository = DependencyResolver
    ::getClassWithDependency('company-contact-repository', [$connection, $companyContactFactory]);

$viewController = new ViewController($companyRepository, $companyContactRepository);

if (isset($_POST['updateCompanyData'])) {
    $viewController->saveEdit($_POST);
    $viewController->showMainView();
} elseif (isset($_POST['edit'])) {
    $viewController->showEditCompanyView($_POST);
} elseif (isset($_POST['delete'])) {
    $viewController->softDelete($_POST);
    $viewController->showMainView();
} elseif (isset($_POST['addCompanyData'])) {
    $viewController->save($_POST);
    $viewController->showMainView();
} elseif (isset($_POST['addCompany'])) {
    $viewController->showAddCompanyView();
} else {
    $viewController->showMainView();
}










