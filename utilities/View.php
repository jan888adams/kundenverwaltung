<?php

/**
 * Class View
 */
class View
{
    /**
     * @var array
     */
    public static $register = [];

    /**
     * @param $route
     * @param $view
     * @param array $arguments
     */
    public static function register($route, $view, $arguments = [])
    {
        self::$register[$route] = self::render($view, $arguments);
    }

    /**
     * @param $route
     */
    public static function load($route)
    {
        $view = self::$register[$route];
        echo $view;
    }

    /**
     * @param $v0738238615_name
     * @param array $arguments
     * @return false|string
     */
    public static function render($v0738238615_name, $arguments = [])
    {
        ob_start();
        if (is_array($arguments)) {
            extract($arguments);
        }

        include('view/' . $v0738238615_name . '.php');
        return ob_get_clean();
    }

}