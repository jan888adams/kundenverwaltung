<?php

DependencyResolver::register('db-connection', 'MySqlConnection');
DependencyResolver::register('company-repository', 'CompanyRepository');
DependencyResolver::register('company-contact-repository', 'CompanyContactRepository');
DependencyResolver::register('company-factory', 'CompanyFactory');
DependencyResolver::register('company-contact-factory', 'CompanyContactFactory');
