
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Anschrift</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <h1>Add a new company</h1>
        <form action="/index.php" method="post" class="form-horizontal">
            <h2>Company Contact information</h2>
            <div class="form-group">
                <label for="firstname" class="control-label col-sm-2">firstname: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Firstname" name="firstname" id="firstname">
                </div>
            </div>

            <div class="form-group">
                <label for="surname" class="control-label col-sm-2">surname:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Surname" name="surname" id="surname">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="control-label col-sm-2">Email:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Email" name="email" id="email">
                </div>
            </div>

            <h2>Company information</h2>

            <div class="form-group">
                <label for="name" class="control-label col-sm-2"> name: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Name" name="companyName" id="name">
                </div>
            </div>

            <div class="form-group">
                <label for="street" class="control-label col-sm-2">street: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Street" name="street" id="street">
                </div>
            </div>

            <div class="form-group">
                <label for="zip"" class="control-label col-sm-2">zip code:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Zip code" name="zip" id="zip">
                </div>
            </div>

            <div class="form-group">
                <label for="segment" class="control-label col-sm-2">segment:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Segment" name="segment" id="segment">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn  btn-primary" type="submit" name="addCompanyData">add</button>
                </div>
            </div>
        </form>

    </div>
</div>

</body>
</html>