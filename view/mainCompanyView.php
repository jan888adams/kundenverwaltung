<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Company</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Company</th>
                <th scope="col">Street</th>
                <th scope="col">Zip</th>
                <th scope="col">Segment</th>
                <th scope="col">Contact</th>
                <th scope="col">
                    <div class="text-center">
                        <form action="index.php" method="post">
                            <button class='btn btn-outline-primary' type='submit' name='addCompany'>Add company</button>
                    </div>
                    </form></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($allCompanies as $company) {
                $contactId = $company['contactId'];
                $companyId = $company['id'];
                echo "<tr>" . "<td>" . $company['name'] . "</td>" . "<td>" . $company['street'] . "</td>" .
                    "<td>" . $company['zip'] . "</td>" . "<td>" . $company['segment'] . "<td>" . $company['contactEmail'] . "</td>" . "</td>"
                    . "<td>" . "<form action='/' method='post'>" . "<input type='hidden' name='contactId' value='$contactId'/>" .
                    "<input type='hidden' name='companyId' value='$companyId'/>" .
                    "<button class='btn btn-outline-danger btn-sm' type='submit' name='delete'>Delete</button>"
                    . "</form>" . "</td>" . "<td>" . "<form action='/' method='post'>" . "<input type='hidden' name='contactId' value='$contactId'/>" .
                    "<input type='hidden' name='companyId' value='$companyId'/>" .
                    "<button class='btn btn-outline-warning ' type='submit' name='edit'>Edit</button>"
                    . "</form>" . "</td>" . "</tr>";
            }
            ?>
            </tbody>

        </table>
    </div>
</div>

</body>
</html>