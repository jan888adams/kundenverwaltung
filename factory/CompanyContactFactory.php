<?php

require_once("FactoryInterface.php");
require_once("model/CompanyContact.php");

class CompanyContactFactory implements FactoryInterface
{
    /**
     * @param array $attributes
     * @return CompanyContact
     */
    public static function create(array $attributes): CompanyContact
    {
        $companyContact = new CompanyContact();
        $companyContact->setId($attributes['id']);
        $companyContact->setFirstname($attributes['firstname']);
        $companyContact->setSurname($attributes['surname']);
        $companyContact->setEmail($attributes['email']);
        $companyContact->setIsDeleted($attributes['is_deleted']);

        return $companyContact;
    }
}
