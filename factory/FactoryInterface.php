<?php


/**
 * Interface IFactory
 */
Interface FactoryInterface
{
    /**
     * @param array $attributes
     * @return mixed
     */
  public static function create(Array $attributes);

}