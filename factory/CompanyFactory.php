<?php

require_once("FactoryInterface.php");
require_once("model/Company.php");

/**
 * Class CompanyFactory
 */
class CompanyFactory implements FactoryInterface
{
    /**
     * @param array $attributes
     * @return Company
     */
    public static function create(array $attributes): Company
    {
        $company = new Company();
        $company->setId($attributes['id']);
        $company->setName($attributes['name']);
        $company->setZip($attributes['zip']);
        $company->setStreet($attributes['street']);
        $company->setSegment($attributes['segment']);
        $company->setContactId($attributes['contact_id']);
        $company->setIsDeleted($attributes['is_deleted']);


        return $company;
    }
}