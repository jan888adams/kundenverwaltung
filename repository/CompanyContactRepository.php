<?php

require_once('repository/RepositoryInterface.php');

/**
 * Class CompanyContactRepository
 */
class CompanyContactRepository implements RepositoryInterface
{

    /**
     * @var string
     */
    private $dataAccess;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * CompanyContactRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $id
     * @return CompanyContact
     */
    public function findById($id): CompanyContact
    {
        $attributes = $this->dataAccess->read('company_contact', ['id' => $id]);

        return $this->factory::create([
            'id' => $attributes[0][0],
            'firstname' => $attributes[0][1],
            'surname' => $attributes[0][2],
            'email' => $attributes[0][3],
            'is_deleted' => $attributes[0][4],
        ]);
    }



    /**
     * @param $id
     * @return mixed|void
     */
    public function delete($id)
    {
        $this->dataAccess->delete('company_contact', ['id' => $id]);
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function save($model)
    {
        $this->dataAccess->save('company_contact', [
            'id' => $model->getId(),
            'firstname' => $model->getFirstname(),
            'surname' => $model->getSurname(),
            'email' => $model->getEmail(),
            'is_deleted' => 0
        ]);
    }

    /**
     * @param $model
     */
    public function update($model)
    {
        $this->dataAccess->update('company_contact', [
            'id' => $model->getId(),
            'name' => $model->getFirstname(),
            'surname' => $model->getSurname(),
            'email' => $model->getEmail(),
            'is_deleted' => 0
        ], ['id' => $model->getId()]);
    }

    public function softDelete($model)
    {
        $this->dataAccess->update('company_contact', [
            'id' => $model->getId(),
            'name' => $model->getFirstname(),
            'surname' => $model->getSurname(),
            'email' => $model->getEmail(),
            'is_deleted' => 1
        ], ['id' => $model->getId()]);
    }

    public function findAll()
    {
        // TODO: Implement findAll() method.
    }
}