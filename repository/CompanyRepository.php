<?php

require_once('repository/RepositoryInterface.php');
require_once('repository/CompanyRepository.php');

/**
 * Class CompanyRepository
 */
class CompanyRepository implements RepositoryInterface
{
    /**
     * @var DataAccessInterface
     */
    private $dataAccess;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * CompanyRepository constructor.
     * @param DataAccessInterface $dataAccess
     * @param FactoryInterface $factory
     */
    public function __construct(DataAccessInterface $dataAccess, FactoryInterface $factory)
    {
        $this->dataAccess = $dataAccess;
        $this->factory = $factory;
    }

    /**
     * @param $id
     * @return Company
     */
    public function findById($id): Company
    {
        $attributes = $this->dataAccess->read('company', ['id' => $id]);

        return $this->factory::create([
            'id' => $attributes[0][0],
            'name' => $attributes[0][1],
            'street' => $attributes[0][2],
            'zip' => $attributes[0][3],
            'segment' => $attributes[0][4],
            'contact_id' => $attributes[0][5],
            'is_deleted' => $attributes[0][6]
        ]);
    }

    /**
     * @return mixed
     */
    public function findAll(): array
    {
        $companies = [];

        $allCompanies = $this->dataAccess->read('company');

        foreach ($allCompanies as $attributes) {
            $companies[] = $this->factory::create([
                'id' => $attributes[0],
                'name' => $attributes[1],
                'street' => $attributes[2],
                'zip' => $attributes[3],
                'segment' => $attributes[4],
                'contact_id' => $attributes[5],
                'is_deleted' => $attributes[6]
            ]);
        }

        return $companies;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->dataAccess->delete('company', ['id' => $id]);
    }

    /**
     * @param $model
     */
    public function save($model)
    {
        $this->dataAccess->save('company', [
            'id' => $model->getId(),
            'name' => $model->getName(),
            'street' => $model->getStreet(),
            'zip' => $model->getZip(),
            'segment' => $model->getSegment(),
            'contact_id' => $model->getContactId(),
            'is_deleted' => 0
        ]);
    }

    /**
     * @param $model
     */
    public function update($model)
    {
        $this->dataAccess->update('company', [
            'id' => $model->getId(),
            'name' => $model->getName(),
            'street' => $model->getStreet(),
            'zip' => $model->getZip(),
            'segment' => $model->getSegment(),
            'contact_id' => $model->getContactId(),
            'is_deleted' => 0
        ], ['id' => $model->getId()]);
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function softDelete($model)
    {
        $this->dataAccess->update('company', [
            'id' => $model->getId(),
            'name' => $model->getName(),
            'street' => $model->getStreet(),
            'zip' => $model->getZip(),
            'segment' => $model->getSegment(),
            'contact_id' => $model->getContactId(),
            'is_deleted' => 1
        ], ['id' => $model->getId()]);


    }
}