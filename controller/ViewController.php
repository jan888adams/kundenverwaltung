<?php

require_once('utilities/View.php');

/**
 * Class ViewController
 */
class ViewController
{

    /**
     * @var RepositoryInterface
     */
    private $companyRepository;
    /**
     * @var RepositoryInterface
     */
    private $companyContactRepository;

    /**
     * ViewController constructor.
     * @param $companyRepository
     * @param $companyContactRepository
     */
    public function __construct(RepositoryInterface $companyRepository, RepositoryInterface $companyContactRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->companyContactRepository = $companyContactRepository;
    }

    /**
     * @param array $request
     */
    public function save(array $request)
    {
        $companyContactId = uniqid();

        $companyContact = CompanyContactFactory::create([
            'firstname' => $request['firstname'],
            'surname' => $request['surname'],
            'email' => $request['email'],
            'id' => $companyContactId,
            'is_deleted' => 0
        ]);

        $company = CompanyFactory::create([
            'name' => $request['companyName'],
            'id' => uniqid(),
            'street' => $request['street'],
            'zip' => $request['zip'],
            'segment' => $request['segment'],
            'contact_id' => $companyContactId,
            'is_deleted' => 0
        ]);

        $this->companyContactRepository->save($companyContact);
        $this->companyRepository->save($company);
    }

    /**
     * @param array $request
     */
    public function saveEdit(array $request)
    {
        $company = $this->companyRepository->findById($request['id']);
        $companyContact = $this->companyContactRepository->findById($request['contactId']);

        $firstname = $request['firstname'] == "" ? $companyContact->getFirstname() : $request['firstname'];
        $contactId = $request['contactId'] == "" ? $companyContact->getId() : $request['contactId'];
        $surname = $request['surname'] == "" ? $companyContact->getSurname() : $request['surname'];
        $email = $request['email'] == "" ? $companyContact->getEmail() : $request['email'];
        $name = $request['companyName'] == "" ? $company->getName() : $request['companyName'];
        $id = $request['id'] == "" ? $company->getId() : $request['id'];
        $street = $request['street'] == "" ? $company->getStreet() : $request['street'];
        $zip = $request['zip'] == "" ? $company->getZip() : $request['zip'];
        $segment = $request['segment'] == "" ? $company->getSegment() : $request['segment'];


        $companyContactAttributes = [
            'firstname' => $firstname,
            'surname' => $surname,
            'email' => $email,
            'id' => $id,
            'is_deleted' => 0
        ];

        $companyAttributes = [
            'name' => $name,
            'id' => $id,
            'street' => $street,
            'zip' => $zip,
            'segment' => $segment,
            'contact_id' => $contactId,
            'is_deleted' => 0
        ];

        $companyContact = CompanyContactFactory::create($companyContactAttributes);
        $company = CompanyFactory::create($companyAttributes);

        $this->companyContactRepository->update($companyContact);
        $this->companyRepository->update($company);
    }

    /**
     * @param $request
     */
    public function delete($request)
    {
        $this->companyRepository->delete($request['companyId']);
        $this->companyContactRepository->delete($request['contactId']);
    }

    /**
     * @param $request
     */
    public function softDelete($request)
    {
        $this->companyRepository->softDelete($this->companyRepository->findById($request['companyId']));
        $this->companyContactRepository->softDelete($this->companyContactRepository->findById($request['contactId']));
    }

    /**
     * display mainCompanyView
     */
    public function showMainView()
    {
        $companies = $this->companyRepository->findAll();
        $fetchedCompanyData = [];

        foreach ($companies as $company) {
            if ($company->getIsDeleted() == 0) {
                $fetchedCompanyData[] = [
                    'id' => $company->getId(),
                    'name' => $company->getName(),
                    'street' => $company->getStreet(),
                    'zip' => $company->getZip(),
                    'segment' => $company->getSegment(),
                    'contactId' => $company->getContactId(),
                    'contactEmail' =>
                        $this->companyContactRepository
                            ->findById($company->getContactId())
                            ->getEmail(),
                ];
            }
        }

        echo View::render('mainCompanyView', [
            'allCompanies' => $fetchedCompanyData,
        ]);
    }

    /**
     * display editCompanyView
     * @param array $request
     */
    public function showEditCompanyView(array $request)
    {
        $company = $this->companyRepository->findById($request['companyId']);
        $companyContact = $this->companyContactRepository->findById($request['contactId']);

        echo View::render('editCompanyView', [
            'name' => $company->getName(),
            'id' => $company->getId(),
            'street' => $company->getStreet(),
            'zip' => $company->getZip(),
            'segment' => $company->getSegment(),
            'contactId' => $company->getContactId(),
            'firstname' => $companyContact->getFirstname(),
            'surname' => $companyContact->getSurname(),
            'email' => $companyContact->getEmail()
        ]);
    }

    /**
     *display addCompanyView
     */
    public function showAddCompanyView()
    {
        echo View::render('addCompanyView');
    }

}